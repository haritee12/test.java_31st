/**
 * 
 */

/**
 * @author haritis
 *
 */

public class Circle_area {

	/**
	 * @param args
	 */
	final static double PI = 3.14;
	public static void main(String[] args) {
		
		double radius = Integer.parseInt(args[0]);
		double area = PI*radius*radius;
		
		//double area = PI*Math.pow(radius, 2);
		
		System.out.println(area);		

	}

}
